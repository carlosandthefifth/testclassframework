# Template
```
/*********************************************************************************************
 * NAME:  Base_TestContactBuilder
 * DESCRIPTION: Test class for American Red Cross Contacts
 * NAME:  Base_TestContactBuilder
 * DESCRIPTION: Test builder class for Contact.
 * MODIFICATION LOG:
 * DEVELOPER                         DATE                               DESCRIPTION
 * _____________________________________________________________________________________________
 * Daniel Gustafson                  10/02/2020                         Created the first version
 *
*************************************************************************************************/
@istest
public class Base_TestContactBuilder {    
    
    boolean recordObjectExist;
 
    /********************************************************************************************
        METHOD NAME    : Base_TestAccountBuilder 
        DESCRIPTION    : constructor set initial values
        PARAMETER      : 
        RETURN Type    : 
    ********************************************************************************************/
    public Base_TestAccountBuilder() {
        recordObjectExist = new Base_Utility().sObjectExist('RecordType');
    }
    
    /********************************************************************************************
     METHOD NAME     : setRecordTypeId
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setRecordTypeId(String recordTypeId) {
    if (this.recordObjectExist) {   
        this.recordTypeName = new Base_TestRecordTypeBuilder().setSobjectName('Contact').setId(recordTypeId).getRecordTypeName();
        this.recordTypeId = recordTypeId;
    }
        return this;
    }
    /********************************************************************************************
         METHOD NAME    : setRecordType
        DESCRIPTION    : setter method
        PARAMETER      : String
        RETURN Type    : Base_TestAccountBuilder
    ********************************************************************************************/ 
    public Base_TestAccountBuilder setRecordTypeName(String recordTypeName) {    
        if (this.recordObjectExist) {   
            this.recordTypeId = new Base_TestRecordTypeBuilder().setSobjectName('Contact').setName(recordTypeName).getRecordTypeId();
            this.recordTypeName = recordTypeName;
        }
        return this;    
    }
    
   /********************************************************************************************
        METHOD NAME    : build
        DESCRIPTION    : bulid the sObject
        PARAMETER      : 
        RETURN Type    : Contact
   ********************************************************************************************/
    public Contact build() {
    sObject conObj = Schema.getGlobalDescribe().get('Contact').newSObject();
        
        if (new Base_Utility().setSObjectName('Contact').canUpdate('CreatedDate')) 
            conObj.put('CreatedDate',this.CreatedDate);
    
        if (new Base_Utility().setSObjectName('Contact').canUpdate('CreatedById'))
            conObj.put('CreatedById',this.CreatedById);

        if (new Base_Utility().setSObjectName('Contact').canUpdate('LastModifiedDate'))            
            conObj.put('LastModifiedDate',this.LastModifiedDate);

        if (new Base_Utility().setSObjectName('Contact').canUpdate('LastModifiedById'))
            conObj.put('LastModifiedById',this.LastModifiedById);
    
        if (new Base_Utility().setSObjectName('Contact').canUpdate('SystemModstamp'))
            conObj.put('SystemModstamp',this.SystemModstamp);

        if (new Base_Utility().setSObjectName('Contact').canUpdate('LastActivityDate'))
            conObj.put('LastActivityDate',this.LastActivityDate);

        if (new Base_Utility().setSObjectName('Contact').canUpdate('LastViewedDate'))
            conObj.put('LastViewedDate',this.LastViewedDate);

        if (new Base_Utility().setSObjectName('Contact').canUpdate('LastReferencedDate'))
            conObj.put('LastReferencedDate',this.LastReferencedDate);

        return (Contact) conObj;
   }
   /********************************************************************************************
        METHOD NAME    : buildAndSave
        DESCRIPTION    : Create sObject record
        PARAMETER      : 
        RETURN Type    : Contact
   ********************************************************************************************/ 
   public Contact buildAndSave() {
      Contact conObj = this.build();
      insert conObj;
      return conObj;
   }
}
```