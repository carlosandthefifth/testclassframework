/*********************************************************************************************
 * NAME:  Base_TestContactBuilder
 * DESCRIPTION: Te/*********************************************************************************************
 * NAME:  Base_TestContactBuilder
 * DESCRIPTION: Test builder class for Contact.
 * MODIFICATION LOG:
 * DEVELOPER                         DATE                               DESCRIPTION
 * _____________________________________________________________________________________________
 * Daniel Gustafson                  09/28/2020                         Created the first version
 *
*************************************************************************************************/
@istest
public class Base_TestContactBuilder {    
 
    private Id AccountId;
    private String AssistantName;
    private String AssistantPhone;
    private Date Birthdate;
    private String CleanStatus;
    private String Department;
    private String Description;
    private String Email;
    private Date EmailBouncedDate;
    private String EmailBouncedReason;
    private String Fax;
    private String FirstName;
    private String HomePhone;
    private Id IndividualId;
    private Boolean IsEmailBounced;
    private String MailingAddress;
    private String MailingCity;
    private String MailingCountry;
    private String MailingGeocodeAccuracy;
    private Double MailingLatitude;
    private Double MailingLongitude;
    private String MailingPostalCode;
    private String MailingState;
    private String MailingStreet;
    private String MobilePhone;
    private String Name;
    private String OtherAddress;
    private String OtherCity;
    private String OtherCountry;
    private String OtherGeocodeAccuracy;
    private Double OtherLatitude;
    private Double OtherLongitude;
    private String OtherPhone;
    private String OtherPostalCode;
    private String OtherState;
    private String OtherStreet;
    private String Phone;
    private String PhotoUrl;
    private Id ReportsToId;
    private String Salutation;
    private String Title;
    
    private Boolean recordTypeExist;
    
    /********************************************************************************************
        METHOD NAME    : Base_TestContactBuilder 
        DESCRIPTION    : constructor set initial values
        PARAMETER      : 
        RETURN Type    : 
    ********************************************************************************************/
    public Base_TestContactBuilder() {
        this.recordTypeExist = new Base_Utility().setSObjectName('Contact').fieldExist('RecordType');
    }
    /********************************************************************************************
        METHOD NAME    : setAccountId
        DESCRIPTION    : seter method
        PARAMETER      : String
        RETURN Type    : Base_TestContactBuilder
    ********************************************************************************************/
    public Base_TestContactBuilder setAccountId(Id accountId) {            
    this.AccountId = accountId;       
    return this;    
    }

    public Base_TestContactBuilder setAssistantName(String assistantName) {
        this.AssistantName = assistantName;
        return this;
    }

    public Base_TestContactBuilder setAssistantPhone(String assistantPhone) {
        this.AssistantPhone = assistantPhone;
        return this;
    }

    public Base_TestContactBuilder setBirthdate(Date birthdate) {
        this.Birthdate = birthdate;
        return this;
    }

    public Base_TestContactBuilder setCleanStatus(String cleanStatus) {
        this.CleanStatus = cleanStatus;
        return this;
    }

    public Base_TestContactBuilder setDepartment(String department) {
        this.Department = department;
        return this;
    }

    public Base_TestContactBuilder setDescription(String description) {
        this.Description = description;
        return this;
    }

    public Base_TestContactBuilder setEmail(String email) {
        this.Email = email;
        return this;
    }

    public Base_TestContactBuilder setEmailBouncedDate(Date emailBouncedDate ) {
        this.EmailBouncedDate = emailBouncedDate;
        return this;
    }

    public Base_TestContactBuilder setEmailBouncedReason(String emailBouncedReason) {
        this.EmailBouncedReason = emailBouncedReason;
        return this;
    }

    public Base_TestContactBuilder setFax(String fax) {
        this.Fax = fax;
        return this;
    }

    public Base_TestContactBuilder setFirstName(String firstName) {
        this.FirstName = firstName;
        return this;
    }

    public Base_TestContactBuilder setHomePhone(String homePhone) {
        this.HomePhone = homePhone;
        return this;
    }

    public Base_TestContactBuilder setIndividualId(Id individualId ) {
        this.IndividualId = individualId;
        return this;
    }

    public Base_TestContactBuilder setIsEmailBounced(String isEmailBounced) {
        this.IsEmailBounced = IsEmailBounced;
        return this;
    }

    public Base_TestContactBuilder setMailingAddress(String mailingAddress) {
        this.MailingAddress = mailingAddress;
        return this;
    }

    public Base_TestContactBuilder setMailingCity(String MailingCity) {
        this.MailingCity = mailingCity;
        return this;
    }

    public Base_TestContactBuilder setMailingCountry(String mailingCountry) {
        this.MailingCountry = mailingCountry;
        return this;
    }

    public Base_TestContactBuilder setMailingGeocodeAccuracy(String mailingGeocodeAccuracy) {
        this.MailingGeocodeAccuracy = mailingGeocodeAccuracy;
        return this;
    }

    public Base_TestContactBuilder setMailingLatitude(String mailingLatitude) {
        this.MailingLatitude = mailingLatitude;
        return this;
    }

    public Base_TestContactBuilder setMailingLongitude(Double MailingLongitude) {
        this.MailingLongitude = mailingLongitude;
        return this;
    }

    public Base_TestContactBuilder setMailingPostalCode(String mailingPostalCode) {
        this.MailingPostalCode = mailingPostalCode;
        return this;
    }

    public Base_TestContactBuilder setMailingState(String mailingState) {
        this.MailingState = mailingState;
        return this;
    }

    public Base_TestContactBuilder setMailingStreet(String mailingStreet) {
        this.MailingStreet = mailingStreet;
        return this;
    }

    public Base_TestContactBuilder setMobilePhone(String mobilePhone) {
        this.MobilePhone = mobilePhone;
        return this;
    }

    public Base_TestContactBuilder setName(String Name) {
        this.FirstName = ;
        this.LastName = ;
        return this;
    }

    public Base_TestContactBuilder set(String ) {
        this. = ;
        return this;
    }

    public Base_TestContactBuilder set(String ) {
        this. = ;
        return this;
    }

    public Base_TestContactBuilder set(String ) {
        this. = ;
        return this;
    }

    public Base_TestContactBuilder set(String ) {
        this. = ;
        return this;
    }

    public Base_TestContactBuilder set(String ) {
        this. = ;
        return this;
    }

    public Base_TestContactBuilder set(String ) {
        this. = ;
        return this;
    }

    public Base_TestContactBuilder set(String ) {
        this. = ;
        return this;
    }

    public Base_TestContactBuilder set(String ) {
        this. = ;
        return this;
    }

    private String Name;
    private String OtherAddress;
    private String OtherCity;
    private String OtherCountry;
    private String OtherGeocodeAccuracy;
    private Double OtherLatitude;
    private Double OtherLongitude;
    private String OtherPhone;
    private String OtherPostalCode;
    private String OtherState;
    private String OtherStreet;
    private String Phone;
    private String PhotoUrl;
    private Id ReportsToId;
    private String Salutation;
    private String Title;

   /********************************************************************************************
        METHOD NAME    : build
        DESCRIPTION    : bulid the sObject
        PARAMETER      : 
        RETURN Type    : Contact
   ********************************************************************************************/
    public Contact build() {

    sObject accObj = Schema.getGlobalDescribe().get('Contact').newSObject() ;
    accObj.put('Name', this.name);
    accObj.put('Phone', this.phone);
    accObj.put('Type', this.type);
    accObj.put('BillingStreet', this.billingStreet);
    accObj.put('BillingCity', this.billingCity);
    accObj.put('BillingState', this.billingState);
    accObj.put('BillingCountry', this.billingCountry);
    accObj.put('BillingPostalCode', this.billingPostalCode);

    if (this.recordTypeExist) {
        accObj.put('RecordTypeId', this.recordTypeId);
        if(this.recordTypeName == 'Person Account') {
            accObj.put('FirstName', this.FirstName);
            accObj.put('LastName', this.LastName);
            accObj.put('Suffix', this.suffix);
        }

    }

        return (Account) accObj;
   }
   /********************************************************************************************
        METHOD NAME    : buildAndSave
        DESCRIPTION    : Create sObject record
        PARAMETER      : 
        RETURN Type    : Account
   ********************************************************************************************/ 
   public Account buildAndSave() {
      Account accObj = this.build();
      insert accObj;
      return accObj;
   }
}