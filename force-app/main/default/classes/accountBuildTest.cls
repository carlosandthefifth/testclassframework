/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 10-03-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   09-27-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
public with sharing class accountBuildTest {
    @TestSetup
    static void testData(){ 
        /*
        Base_ExecutionControlSetting__c myCustomSetting  = new VAMS_ExecutionControlSetting__c();
        myCustomSetting.VAMS_Run_Trigger__c = true;
        myCustomSetting.VAMS_Run_Process_Builder__c = true;
        myCustomSetting.VAMS_Run_Validation_Rule__c = true;
        myCustomSetting.VAMS_Run_Workflow__c = true;
        insert myCustomSetting;
        */
        Account testAccount = new Base_TestAccountBuilder().setName('test').build();
        insert testAccount;
    }

    @IsTest
    static void getAccount(){
        User adminUser = getActiveAdminUser('System Administrator');
        system.debug('adminUser: ' + adminUser);
        Test.startTest();
        System.runAs(adminUser){
           system.debug('account: ' + getAccountByName('test'));
        }

        Test.stopTest();
        
    }

    public static Account getAccountByName(String Name) {
        String query;
        if (new Base_Utility().sObjectExist('RecordType'))
            query = 'SELECT Name,Id, recordTypeId FROM Account WHERE Name = \'' + Name + '\'';
        else 
            query = 'SELECT Name,Id FROM Account WHERE Name = \'' + Name + '\'';
        system.debug('query: ' + query);
        return Database.query(query);     
    }

    public static User getActiveAdminUser(String adminProfile) {
        return [SELECT Id FROM User WHERE Profile.Name = :adminProfile AND isActive = true];
    }
}
