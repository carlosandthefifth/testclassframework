/*********************************************************************************************
 * NAME:  Base_TestRecordTypeBuilder
 * DESCRIPTION: Test class for American Red Cross Recordtypes
 * NAME:  Base_TestRecordTypeBuilder
 * DESCRIPTION: Test builder class for Account.
 * MODIFICATION LOG:
 * DEVELOPER                         DATE                               DESCRIPTION
 * _____________________________________________________________________________________________
 * Daniel Gustafson                  10/02/2020                         Created the first version
 *
*************************************************************************************************/
@IsTest
public class Base_TestRecordTypeBuilder {

    public class testRecordTypeBuilderException extends Exception {}

    private enum filteringByType {FILTERBYID, FILTERBYNAME}

    private String sObjectName;
    private String recordTypeName;
    private String recordTypeId;
    private RecordType recordType;

    private Boolean filterById   = false;
    private Boolean filterByName = false;

    public Base_TestRecordTypeBuilder() {}

    public Base_TestRecordTypeBuilder setSobjectName(String name) {
        this.sObjectName = name;
        return this;
    }

    public Base_TestRecordTypeBuilder setName(String name) {
        this.recordTypeName = name;
        setFilter(filteringByType.FILTERBYNAME);
        return this;
    }

    public Base_TestRecordTypeBuilder setId(String Id) {
        this.recordTypeId = Id;
        setFilter(filteringByType.FILTERBYID);
        return this;
    }

    public String  getRecordTypeId() {
        String query = buildQuery();
        system.debug('query: ' + query);
        try {
            RecordType result = Database.query(query);
            return result.Id;
        } catch(QueryException e) {
            system.debug('ACR_TestRecordTypeBuilder.getRecordTypeId exception message: ' + e.getMessage());
        }
        return '';
    }

    public String  getRecordTypeName() {
        String query = buildQuery();

        try {
            RecordType result = Database.query(query);
            return result.Name;
        } catch(QueryException e) {
            system.debug('ACR_TestRecordTypeBuilder.getRecordTypeName exception message: ' + e.getMessage());
        }
        return '';
    }

    public RecordType getRecordType() {
        RecordType result = null;
        try {   
            String query = buildQuery();

            result = Database.query(query);
        } catch(QueryException e) {
            system.debug('ACR_TestRecordTypeBuilder.getRecordType exception message: ' + e.getMessage());
        }
        return result;
    }

    private void setFilter(filteringByType filteringBy) {
        this.filterById   = false;
        this.filterByName = false;    
        switch on filteringBy {
            When FILTERBYID {
                this.filterById   = true;        
            }
            When FILTERBYNAME {
                this.filterByName = true;    
            }
        }

    }

    private String buildQuery () {

        String query = 'SELECT BusinessProcessId,CreatedById,CreatedDate,Description,DeveloperName,Id,IsActive,LastModifiedById,LastModifiedDate,Name,NamespacePrefix,SobjectType,SystemModstamp FROM RecordType {0} AND {1}';

        List<String> parameters = new List<String>();
        try {
            if (filterById) {
                parameters.add(' WHERE Id = ' + '\'' + this.RecordTypeId + '\'');
            } else if (filterByName) {
                parameters.add(' WHERE Name =' + '\'' + this.RecordTypeName + '\'');
            }
            else {
                throw new testRecordTypeBuilderException('filter not set');
            }

            parameters.add('SobjectType = ' + '\'' + this.sObjectName + '\'');
            
        } catch (Exception e) {
            system.debug('ACR_TestRecordTypeBuilder.buildQuery exception message: ' + e.getMessage());
        }

        return String.Format(query,parameters);

    }
}
