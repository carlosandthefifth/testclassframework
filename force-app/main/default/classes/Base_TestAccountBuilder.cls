/*********************************************************************************************
 * NAME:  Base_TestAccountBuilder
 * DESCRIPTION: Te/*********************************************************************************************
 * NAME:  Base_TestAccountBuilder
 * DESCRIPTION: Test builder class for Account.
 * MODIFICATION LOG:
 * DEVELOPER                         DATE                               DESCRIPTION
 * _____________________________________________________________________________________________
 * Daniel Gustafson                  09/17/2020                         Created the first version
 *
*************************************************************************************************/
@istest
public class Base_TestAccountBuilder {    
    private Id Id;
    private String Name;
    private String Type;
    private String ParentId;
    private String BillingStreet;
    private String BillingCity;
    private String BillingState;
    private String BillingPostalCode;
    private String BillingCountry;
    private Double BillingLatitude;
    private Double BillingLongitude;
    private String BillingGeocodeAccuracy;
    private String ShippingStreet;
    private String ShippingCity;
    private String ShippingState;
    private String ShippingPostalCode;
    private String ShippingCountry;
    private Double ShippingLatitude;
    private Double ShippingLongitude;
    private String ShippingGeocodeAccuracy;
    private String Phone;
    private String Fax;
    private String AccountNumber;
    private String Website;
    private String Sic;
    private String Industry;
    private Double AnnualRevenue;
    private Integer NumberOfEmployees;
    private String Ownership;
    private String TickerSymbol;
    private String Description;
    private String Rating;
    private String Site;
    private String OwnerId;
    private Datetime CreatedDate;
    private String CreatedById;
    private Datetime LastModifiedDate;
    private String LastModifiedById;
    private Datetime SystemModstamp;
    private Date LastActivityDate;
    private Datetime LastViewedDate;
    private Datetime LastReferencedDate;
    private String Jigsaw;
    private String JigsawCompanyId;
    private String CleanStatus;
    private String AccountSource;
    private String DunsNumber;
    private String Tradestyle;
    private String NaicsCode;
    private String NaicsDesc;
    private String YearStarted;
    private String SicDesc;
    private String DandbCompanyId;
    private String OperatingHoursId;
    private String FirstName;
    private String LastName;
    private String Suffix;
    private RecordType RecordType;
    private String RecordTypeId;
    private String RecordTypeName;


   /********************************************************************************************
      METHOD NAME    : setRecordType
      DESCRIPTION    : setter method
      PARAMETER      : String
      RETURN Type    : Base_TestAccountBuilder
   ********************************************************************************************/ 
   public Base_TestAccountBuilder setRecordType(RecordType recordType) {    
      if (new Base_Utility().sObjectExist('RecordType')) {   
          this.RecordTypeId = new Base_TestRecordTypeBuilder().setSobjectName('Account').setName(recordType.Name).getRecordTypeId();
          this.RecordTypeName = recordTypeName;
      }
      return this;    
   }
   /********************************************************************************************
         METHOD NAME    : setRecordType
         DESCRIPTION    : setter method
         PARAMETER      : String
         RETURN Type    : Base_TestAccountBuilder
   ********************************************************************************************/ 
   public Base_TestAccountBuilder setRecordTypeName(String recordTypeName) {    
         if (new Base_Utility().sObjectExist('RecordType')) {   
            this.recordTypeId = new Base_TestRecordTypeBuilder().setSobjectName('Account').setName(recordTypeName).getRecordTypeId();
            this.recordTypeName = recordTypeName;
         }
         return this;    
   }
    /********************************************************************************************
    METHOD NAME     : setId
    DESCRIPTION     : Setter Method
    PARAMETER       : Id
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setId(Id id) {
       this.Id = id;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setName
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setName(String name) {
       this.Name = name;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setType
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setType(String type) {
       this.Type = type;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setParentId
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setParentId(String parentId) {
       this.ParentId = parentId;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setBillingStreet
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setBillingStreet(String billingStreet) {
       this.BillingStreet = billingStreet;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setBillingCity
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setBillingCity(String billingCity) {
       this.BillingCity = billingCity;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setBillingState
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setBillingState(String billingState) {
       this.BillingState = billingState;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setBillingPostalCode
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setBillingPostalCode(String billingPostalCode) {
       this.BillingPostalCode = billingPostalCode;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setBillingCountry
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setBillingCountry(String billingCountry) {
       this.BillingCountry = billingCountry;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setBillingLatitude
    DESCRIPTION     : Setter Method
    PARAMETER       : Double
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setBillingLatitude(Double billingLatitude) {
       this.BillingLatitude = billingLatitude;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setBillingLongitude
    DESCRIPTION     : Setter Method
    PARAMETER       : Double
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setBillingLongitude(Double billingLongitude) {
       this.BillingLongitude = billingLongitude;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setBillingGeocodeAccuracy
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setBillingGeocodeAccuracy(String billingGeocodeAccuracy) {
       this.BillingGeocodeAccuracy = billingGeocodeAccuracy;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setShippingStreet
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setShippingStreet(String shippingStreet) {
       this.ShippingStreet = shippingStreet;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setShippingCity
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setShippingCity(String shippingCity) {
       this.ShippingCity = shippingCity;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setShippingState
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setShippingState(String shippingState) {
       this.ShippingState = shippingState;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setShippingPostalCode
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setShippingPostalCode(String shippingPostalCode) {
       this.ShippingPostalCode = shippingPostalCode;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setShippingCountry
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setShippingCountry(String shippingCountry) {
       this.ShippingCountry = shippingCountry;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setShippingLatitude
    DESCRIPTION     : Setter Method
    PARAMETER       : Double
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setShippingLatitude(Double shippingLatitude) {
       this.ShippingLatitude = shippingLatitude;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setShippingLongitude
    DESCRIPTION     : Setter Method
    PARAMETER       : Double
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setShippingLongitude(Double shippingLongitude) {
       this.ShippingLongitude = shippingLongitude;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setShippingGeocodeAccuracy
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setShippingGeocodeAccuracy(String shippingGeocodeAccuracy) {
       this.ShippingGeocodeAccuracy = shippingGeocodeAccuracy;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setPhone
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setPhone(String phone) {
       this.Phone = phone;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setFax
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setFax(String fax) {
       this.Fax = fax;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setAccountNumber
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setAccountNumber(String accountNumber) {
       this.AccountNumber = accountNumber;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setWebsite
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setWebsite(String website) {
       this.Website = website;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setSic
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setSic(String sic) {
       this.Sic = sic;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setIndustry
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setIndustry(String industry) {
       this.Industry = industry;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setAnnualRevenue
    DESCRIPTION     : Setter Method
    PARAMETER       : Double
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setAnnualRevenue(Double annualRevenue) {
       this.AnnualRevenue = annualRevenue;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setNumberOfEmployees
    DESCRIPTION     : Setter Method
    PARAMETER       : Integer
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setNumberOfEmployees(Integer numberOfEmployees) {
       this.NumberOfEmployees = numberOfEmployees;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setOwnership
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setOwnership(String ownership) {
       this.Ownership = ownership;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setTickerSymbol
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setTickerSymbol(String tickerSymbol) {
       this.TickerSymbol = tickerSymbol;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setDescription
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setDescription(String description) {
       this.Description = description;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setRating
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setRating(String rating) {
       this.Rating = rating;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setSite
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setSite(String site) {
       this.Site = site;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setOwnerId
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setOwnerId(String ownerId) {
       this.OwnerId = ownerId;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setCreatedDate
    DESCRIPTION     : Setter Method
    PARAMETER       : Datetime
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setCreatedDate(Datetime createdDate) {
       this.CreatedDate = createdDate;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setCreatedById
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setCreatedById(String createdById) {
       this.CreatedById = createdById;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setLastModifiedDate
    DESCRIPTION     : Setter Method
    PARAMETER       : Datetime
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setLastModifiedDate(Datetime lastModifiedDate) {
       this.LastModifiedDate = lastModifiedDate;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setLastModifiedById
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setLastModifiedById(String lastModifiedById) {
       this.LastModifiedById = lastModifiedById;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setSystemModstamp
    DESCRIPTION     : Setter Method
    PARAMETER       : Datetime
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setSystemModstamp(Datetime systemModstamp) {
       this.SystemModstamp = systemModstamp;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setLastActivityDate
    DESCRIPTION     : Setter Method
    PARAMETER       : Date
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setLastActivityDate(Date lastActivityDate) {
       this.LastActivityDate = lastActivityDate;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setLastViewedDate
    DESCRIPTION     : Setter Method
    PARAMETER       : Datetime
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setLastViewedDate(Datetime lastViewedDate) {
       this.LastViewedDate = lastViewedDate;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setLastReferencedDate
    DESCRIPTION     : Setter Method
    PARAMETER       : Datetime
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setLastReferencedDate(Datetime lastReferencedDate) {
       this.LastReferencedDate = lastReferencedDate;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setJigsaw
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setJigsaw(String jigsaw) {
       this.Jigsaw = jigsaw;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setJigsawCompanyId
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setJigsawCompanyId(String jigsawCompanyId) {
       this.JigsawCompanyId = jigsawCompanyId;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setCleanStatus
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setCleanStatus(String cleanStatus) {
       this.CleanStatus = cleanStatus;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setAccountSource
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setAccountSource(String accountSource) {
       this.AccountSource = accountSource;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setDunsNumber
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setDunsNumber(String dunsNumber) {
       this.DunsNumber = dunsNumber;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setTradestyle
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setTradestyle(String tradestyle) {
       this.Tradestyle = tradestyle;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setNaicsCode
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setNaicsCode(String naicsCode) {
       this.NaicsCode = naicsCode;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setNaicsDesc
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setNaicsDesc(String naicsDesc) {
       this.NaicsDesc = naicsDesc;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setYearStarted
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setYearStarted(String yearStarted) {
       this.YearStarted = yearStarted;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setSicDesc
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setSicDesc(String sicDesc) {
       this.SicDesc = sicDesc;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setDandbCompanyId
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setDandbCompanyId(String dandbCompanyId) {
       this.DandbCompanyId = dandbCompanyId;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setOperatingHoursId
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setOperatingHoursId(String operatingHoursId) {
       this.OperatingHoursId = operatingHoursId;
       return this;
    }
    /********************************************************************************************
    METHOD NAME     : setFirstName
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setFirstName(String firstName) {
      this.FirstName = firstName;
      return this;
   }
    /********************************************************************************************
    METHOD NAME     : setLastName
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setLastName(String lastName) {
      this.LastName = lastName;
      return this;
   }
    /********************************************************************************************
    METHOD NAME     : setSuffix
    DESCRIPTION     : Setter Method
    PARAMETER       : String
    RETURN Type     : Base_TestAccountBuilder
    ********************************************************************************************/
    public Base_TestAccountBuilder setSuffix(String suffix) {
      this.Suffix = suffix;
      return this;
   }
     
   /********************************************************************************************
        METHOD NAME    : build
        DESCRIPTION    : bulid the sObject
        PARAMETER      : 
        RETURN Type    : Account
   ********************************************************************************************/
   public Account build() {
   
      sObject accObj = Schema.getGlobalDescribe().get('Account').newSObject() ;


      accObj.put('Id',this.Id);
      accObj.put('Name',this.Name);
      accObj.put('Type',this.Type);
      accObj.put('ParentId',this.ParentId);
      accObj.put('BillingStreet',this.BillingStreet);
      accObj.put('BillingCity',this.BillingCity);
      accObj.put('BillingState',this.BillingState);
      accObj.put('BillingPostalCode',this.BillingPostalCode);
      accObj.put('BillingCountry',this.BillingCountry);
      accObj.put('BillingLatitude',this.BillingLatitude);
      accObj.put('BillingLongitude',this.BillingLongitude);
      accObj.put('BillingGeocodeAccuracy',this.BillingGeocodeAccuracy);
      accObj.put('ShippingStreet',this.ShippingStreet);
      accObj.put('ShippingCity',this.ShippingCity);
      accObj.put('ShippingState',this.ShippingState);
      accObj.put('ShippingPostalCode',this.ShippingPostalCode);
      accObj.put('ShippingCountry',this.ShippingCountry);
      accObj.put('ShippingLatitude',this.ShippingLatitude);
      accObj.put('ShippingLongitude',this.ShippingLongitude);
      accObj.put('ShippingGeocodeAccuracy',this.ShippingGeocodeAccuracy);
      accObj.put('Phone',this.Phone);
      accObj.put('Fax',this.Fax);
      accObj.put('AccountNumber',this.AccountNumber);
      accObj.put('Website',this.Website);
      accObj.put('Sic',this.Sic);
      accObj.put('Industry',this.Industry);
      accObj.put('AnnualRevenue',this.AnnualRevenue);
      accObj.put('NumberOfEmployees',this.NumberOfEmployees);
      accObj.put('Ownership',this.Ownership);
      accObj.put('TickerSymbol',this.TickerSymbol);
      accObj.put('Description',this.Description);
      accObj.put('Rating',this.Rating);
      accObj.put('Site',this.Site);
    
      if (String.isBlank(this.OwnerId)) {
         this.OwnerId = UserInfo.getUserId();
      }
      accObj.put('OwnerId',this.OwnerId);

      if (new Base_Utility().setSObjectName('Account').canUpdate('CreatedDate'))
         accObj.put('CreatedDate',this.CreatedDate);

      if (new Base_Utility().setSObjectName('Account').canUpdate('CreatedById'))
         accObj.put('CreatedById',this.CreatedById);
      if (new Base_Utility().setSObjectName('Account').canUpdate('LastModifiedDate'))
         accObj.put('LastModifiedDate',this.LastModifiedDate);
      if (new Base_Utility().setSObjectName('Account').canUpdate('LastModifiedById'))
         accObj.put('LastModifiedById',this.LastModifiedById);
      if (new Base_Utility().setSObjectName('Account').canUpdate('SystemModstamp'))
         accObj.put('SystemModstamp',this.SystemModstamp);
      if (new Base_Utility().setSObjectName('Account').canUpdate('LastActivityDate'))
        accObj.put('LastActivityDate',this.LastActivityDate);
      if (new Base_Utility().setSObjectName('Account').canUpdate('LastViewedDate'))
         accObj.put('LastViewedDate',this.LastViewedDate);
      if (new Base_Utility().setSObjectName('Account').canUpdate('LastReferencedDate'))  
         accObj.put('LastReferencedDate',this.LastReferencedDate);
      accObj.put('Jigsaw',this.Jigsaw);
      if (new Base_Utility().setSObjectName('Account').canUpdate('JigsawCompanyId'))  
         accObj.put('JigsawCompanyId',this.JigsawCompanyId);
      
      accObj.put('CleanStatus',this.CleanStatus);
      accObj.put('AccountSource',this.AccountSource);
      accObj.put('DunsNumber',this.DunsNumber);
      accObj.put('Tradestyle',this.Tradestyle);
      accObj.put('NaicsCode',this.NaicsCode);
      accObj.put('NaicsDesc',this.NaicsDesc);
      accObj.put('YearStarted',this.YearStarted);
      accObj.put('SicDesc',this.SicDesc);
      accObj.put('DandbCompanyId',this.DandbCompanyId);
      accObj.put('OperatingHoursId',this.OperatingHoursId);

    if (new Base_Utility().sObjectExist('RecordType')) {
        accObj.put('RecordTypeId', this.recordTypeId);
        if(this.recordTypeName == 'Person Account') {
            accObj.put('FirstName', this.FirstName);
            accObj.put('LastName', this.LastName);
            accObj.put('Suffix', this.suffix);
        }

    }
        return (Account) accObj;
   }
   /********************************************************************************************
        METHOD NAME    : buildAndSave
        DESCRIPTION    : Create sObject record
        PARAMETER      : 
        RETURN Type    : Account
   ********************************************************************************************/ 
   public Account buildAndSave() {
      Account accObj = this.build();
      insert accObj;
      return accObj;
   }
}