/**
 * @description       : American Red Cross (ACR) uiltity class 
 * @author            : Daniel Gustsafson
 * @last modified on  : 10-03-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   09-28-2020   Daniel Gustafson   Initial Version
**/
public with sharing class Base_Utility {
    private String sObjectAPIName;
    public Base_Utility() {}

    /********************************************************************************************
        METHOD NAME    : canUpdate
        DESCRIPTION    : true if we can update field
        PARAMETER      : String
        RETURN Type    : Boolean
    ********************************************************************************************/  
    public Boolean canUpdate(String fieldName) {
        // does the field exist AND do we have the permissions to udpate

        Map<String,Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(this.sObjectAPIName).getDescribe().fields.getMap();

        if (fieldExist(fieldName)) {
            return fieldMap.get(fieldName).getDescribe().isUpdateable();
        }
        
        return false;
    }

    /********************************************************************************************
        METHOD NAME    : fieldExist
        DESCRIPTION    : true if field exist
        PARAMETER      : String
        RETURN Type    : Boolean
    ********************************************************************************************/  
    public Boolean fieldExist(String fieldName) {
        SObject anSObject = Schema.getGlobalDescribe().get(this.sObjectAPIName).newSObject();
        return anSObject.getSobjectType().getDescribe().fields.getMap().containsKey(fieldName);
    }
    /********************************************************************************************
        METHOD NAME    : objectExist
        DESCRIPTION    : true if field exist
        PARAMETER      : String
        RETURN Type    : Boolean
    ********************************************************************************************/  
    public Boolean sObjectExist(String objectAPIName) {
        Boolean exist = true;
        try {
            String queryString = 'SELECT id FROM %1 LIMIT 1';
            List<SObject> records = Database.query(queryString);
        } catch(QueryException e) {
            exist = false;
        }
        return exist;
    }
    /********************************************************************************************
        METHOD NAME    : setSObjectName
        DESCRIPTION    : setter method
        PARAMETER      : String
        RETURN Type    : Base_Utility
    ********************************************************************************************/  
    public Base_Utility setSObjectName(String APIName) {
        this.sObjectAPIName = APIName;
        return this;
    }

}
